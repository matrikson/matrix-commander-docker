FROM fedora:latest
RUN dnf -y --setopt=fastestmirror=true upgrade && \
    dnf install -y pip libolm-devel gcc python-devel python3-dbus && \
    dnf clean all
WORKDIR /app
RUN curl -OL https://raw.githubusercontent.com/8go/matrix-commander/master/requirements.txt
RUN curl -OL https://raw.githubusercontent.com/8go/matrix-commander/master/matrix-commander.py
RUN pip3 install -r /app/requirements.txt
VOLUME /data
ENTRYPOINT ["/bin/python3", "/app/matrix-commander.py" ,"-s", "/data/store", "-c", "/data/credentials.json"]
